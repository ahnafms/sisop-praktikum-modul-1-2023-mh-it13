#!/bin/sh
#define functions
INPUT='2023 QS World University Rankings.csv'

[ ! -f "$INPUT" ] && {
    echo "$INPUT file not found"
    exit 99
}

universities=$(cat "$INPUT")
sort_universities=$(echo "$universities" | tr -d '"' | tail -n +2)
jp_top_five=$(echo "$sort_universities" | awk '/JP/ {print}' | head -n +5)

# Nomor 1
echo -e "\nPoin 1\nBocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.\n"
echo -e "$jp_top_five\n"

# Nomor 2
echo -e "Poin 2\nKarena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang."
jp_lowest_fsr=$(echo "$sort_universities" | awk '/JP/ {print}' | sort -t ',' -k 9n | awk -F ',' '{print $1 " " $2 " " $3 " " $9}' | head -n +5)
echo -e "\n$jp_lowest_fsr\n"

# Nomor 3
echo -e "Poin 3\nKarena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.\n"
jp_top_ten_ger=$(echo "$sort_universities" | awk '/JP/ {print}' | sort -t ',' -k 20n | head -n 10 | awk -F ',' '{print $1 " " $2 " " $3 " " $20}')
echo -e "$jp_top_ten_ger\n\nTerendah : $(echo "$jp_top_ten_ger" | head -n 1)\n"

# Nomor 4
echo -e "Poin 4\nBocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren."
keren_univ=$(echo "$sort_universities" | awk '/Keren/ {print}')
echo "$keren_univ"
