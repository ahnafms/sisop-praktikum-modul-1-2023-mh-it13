# sisop-praktikum-modul-1-2023-MH-IT13
## Daftar isi ##
- [Anggota Kelompok](#anggota-kelompok)
- [Nomor 1](#nomor-1)
    - [Soal  1.A](#1a)
    - [Soal  1.B](#1b)
    - [Soal  1.C](#1c)
    - [Soal  1.D](#1d)
- [Nomor 2](#nomor-2)
    - [Soal  2.A](#2a)
    - [Soal  2.B](#2b)
- [Nomor 3](#nomor-3)
    - [Soal  3.A](#3a)
    - [Soal  3.B](#3b)
    - [Hasil](#3hasil)
- [Nomor 4](#nomor-4)
    - [Soal  4.A](#4a)
    - [Soal  4.B](#4b)
    - [Hasil](#4hasil)
- [Kendala](#kendala)


## Anggota Kelompok

| NRP        | NAMA                       |
| ---------- | -------------------------- |
| 5027211038 | Ahnaf Musyaffa             |
| 5027211051 | Wisnu Adjie Saka           |
| 5027211062 | Anisa Ghina Salsabila      |

## Nomor 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  :

### 1.A
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

```bash
sort_universities=$(echo "$universities" | tr -d '"' | tail -n +2)
...
```
Pada potongan kode di `university_survey.sh` ini untuk menghilangkan string yang mengapit data tertentu. kemudian mengambil seluruh data kecuali 1 teratas yakni nama kolom

```bash 
jp_top_five=$(echo "$sort_universities" | awk '/JP/ {print}' | head -n +5)
...
```
Pada potongan kode di `university_survey.sh` untuk ngeprint universitas yang locationnya di jepang kemudian diambil 5 teratas. Karena sudah tersort namun jika belum tersort maka ditambahkan `sort -t ',' -k 1n` dalam pipe diatas.
### 1.B
Karena Bocchi kurang percaya diri, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang.

```bash
...
jp_lowest_fsr=$(echo "$sort_universities" | awk '/JP/ {print}' | sort -t ',' -k 9n | awk -F ',' '{print $1 " " $2 " " $3 " " $9}' | head -n +5)
...
```
Pada potongan kode di `university_survey.sh` digunakan sort dengan delimiter ',' kemudian diambil variabel ke-9 setelah di sort di print hanya variabel 1 (rank), 2 (nama univ), 3 (lokasi), 9 (fsr score) dan diambil 5 teratas (terendah).

### 1.C
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.

```bash
...
jp_top_ten_ger=$(echo "$sort_universities" | awk '/JP/ {print}' | sort -t ',' -k 20n | head -n 10 | awk -F ',' '{print $1 " " $2 " " $3 " " $20}')
...
```
Pada potongan kode di `university_survey.sh` digunakan sort dengan delimiter  ',' kemudian variable ke 20 di sort dan diambil 10 teratas.

### 1.D
Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

```bash
...
keren_univ=$(echo "$sort_universities" | awk '/Keren/ {print}')
...
```
Pada potongan kode di `university_survey.sh` menggunakan command awk untuk mencari kata 'Keren' pada `$sort_universities` kemudian di print. 

### 1.Hasil
Berikut merupakan hasil dari run `university_survey.sh` dengan username ahnaf:

![Screenshot_2023-03-03_at_00.14.02](/uploads/d2e40ddf4e7e325c67a79cc877820c95/Screenshot_2023-03-03_at_00.14.02.png)

## Nomor 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut.

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan.

Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
### 2.A
File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
```bash
if [ $(find . -type d -name "kumpulan_*" | wc -l) -gt 0 ]; then
    latest_kumpulan=$(( $(ls -1 | awk -F '_' '/kumpulan/ {print $2}' | wc -l ) + 1 ))
else 
    latest_kumpulan=1
fi
```
Pada potongan kode di `kobeni_liburan.sh` mencari folder yang memiliki awalan kumpulan_ dengan tipe direktori.
```bash
(crontab -l; echo "0 */10 * * * if [ \$(ls "$curr_dir/kumpulan_$latest_kumpulan" | wc -l) -lt $curr_hour ]; then wget -O $curr_dir/kumpulan_$latest_kumpulan/perjalanan_\$(( \$(ls -1 "$curr_dir/kumpulan_$latest_kumpulan" | wc -l) + 1 )).jpeg $img_url; fi") | crontab -
```
Pada potongan kode di `kobeni_liburan.sh` menjalankan `crontab -l` yang dimana akan mengeluarkan list cronjobs yang tersedia. Kemudian dijalankan command echo untuk insert cronjob baru yaitu `0 */10 * * *` yang akan berjalan selama 10 jam sekali. Agar cronjob berjalan secara terus-menerus maka diperlukan command `ls /path |wc -l` pada suatu kumpulan untuk mengecek jumlah perjalanan yang telah terdownload. Kemudian menjalankan command `wget` untuk mengunduh image di browser dan flag `-O` untuk mengganti nama file menjadi perjalanan_`$jumlah+1`.

syntax `\$` digunakan agar apapun yang didalam `\$()` akan tereksekusi saat cronjob aktif.
### 2.B
File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) v

```bash
(crontab -l; echo "0 0 * * * cd $curr_dir && zip -j devil_$latest_kumpulan.zip kumpulan_$latest_kumpulan/*") | crontab -
```
Pada potongan kode di `kobeni_liburan.sh` menjalankan `crontab -l` yang dimana akan mengeluarkan list cronjobs yang tersedia. Kemudian dijalankan command echo untuk insert cronjob baru yaitu `0 0 * * *` yang akan berjalan setiap 00:00 AM. `zip -j` digunakan untuk menzip file dengan flag `-j` tidak memasukkan path parent dari kumpulan.

### 2.Hasil
Berikut merupakan isi dari crontab setelah menjalankan command `bash kobeni_liburan.sh`;

![Screenshot_2023-03-03_at_11.34.49](/uploads/e3a463dbb19821e2c8af02c1f312ec87/Screenshot_2023-03-03_at_11.34.49.png)

## Nomor 3
1. Membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt
2. Membuat sistem login yang dibuat di script retep.sh
3. Untuk ketentuan passwordnya pada register dan login memiliki ketentuan berikut :
Minimal 8 karakter, Memiliki minimal 1 huruf kapital dan 1 huruf kecil, Alphanumeric,Tidak boleh sama dengan username, Tidak boleh menggunakan kata chicken atau ernie
4. Percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log :
Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists ; Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully ; Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME ; Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in



### 3.A
1. Membuat folder users terlebih dahulu, lalu membuat file users.txt dan log.txt didalam folder users

Untuk syntax membuat folder : $ mkdir users

Untuk syntax membuat file : $ touch users.txt dan $ touch log.txt

2. Membuat script didalam folder users dengan syntax :

louis.sh : $ nano louis.sh, setelah isi scriptnya, simpan dan ubah permission file script agar dapat dieksekusi dengan cara $ chmod +x louis.sh

retep.sh : $ nano retep.sh, setelah isi scriptnya, simpan dan ubah permission file script agar dapat dieksekusi dengan cara $ chmod +x retep.sh

Untuk me-eksekusi script jalankan dengan syntax :  ./nama_file.sh




### 3.B
Untuk scriptnya :

louis.sh :
```bash
#!/bin/bash

# Input username
echo "Enter username:"
read username

# Periksa apakah nama pengguna sudah ada di users.txt
if grep -q "^${username}:" ~/users/users.txt; then
    echo "Username already exists."
    echo "REGISTER: ERROR User already exists $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Masukkan kata sandi dan periksa apakah memenuhi persyaratan

while true; do
    echo "Enter password (minimum 8 characters, at least 1 uppercase and 1 lowercase letter, alphanumeric, cannot contain username, chicken, or ernie):"
    read -s password

    # Periksa panjang password
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long."
        continue
    fi

    # Periksa setidaknya ada 1 huruf besar di password
    if ! [[ ${password} =~ [A-Z] ]]; then
        echo "Password must contain at least 1 uppercase letter."
        continue
    fi

    # Periksa setidaknya 1 huruf kecil di password
    if ! [[ ${password} =~ [a-z] ]]; then
        echo "Password must contain at least 1 lowercase letter."
        continue
    fi

    # Periksa karakter alfanumerik 

    if ! [[ ${password} =~ ^[a-zA-Z0-9]+$ ]]; then
        echo "Password must contain alphanumeric characters only."
        continue
    fi

    # Periksa apakah password berisi nama pengguna, ayam, atau ernie
    if [[ ${password} =~ ${username} || ${password} =~ "chicken" || ${password} =~ "ernie" ]]; then
        echo "Password cannot contain username, chicken, or ernie."
        continue
    fi

    break
done

# Tambahkan pengguna ke users.txt
echo "${username}:${password}" >> ~/users/users.txt
echo "User ${username} has been registered successfully."
echo "REGISTER: INFO User ${username} registered successfully $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
```
retep.sh :
```bash
#!/bin/bash

# Input username
echo "Enter username:"
read username

# Periksa apakah nama pengguna ada di users.txt

if ! grep -q "^${username}:" ~/users/users.txt; then
    echo "Username does not exist."
    echo "LOGIN: ERROR Failed login attempt on user ${username} $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Input password
echo "Enter password:"
read -s password

# Periksa apakah kata sandi cocok dengan yang ada di users.txt
if ! grep -q "^${username}:${password}$" ~/users/users.txt; then
    echo "Password is incorrect."
    echo "LOGIN: ERROR Failed login attempt on user ${username} $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Login berhasil
echo "Welcome, ${username}!"
echo "LOGIN: INFO User ${username} logged in $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt

```
### 3.Hasil
Untuk registernya di louis.sh:
1. Password sesuai dengan kondisi yang ditentukan 
![Screenshot_2023-03-03_162818](/uploads/17ef326a49cae43bc61ba74b54ae30cd/Screenshot_2023-03-03_162818.png)
2. Password tidak sesuai 
![Screenshot_2023-03-03_180103](/uploads/660b3886e16630b68d148bdd36460ffd/Screenshot_2023-03-03_180103.png)

Cek username apakah sudah terdata pada users.txt: 

![Screenshot_2023-03-03_180349](/uploads/7fc14c413e56bd3c19eb09c3c6413df3/Screenshot_2023-03-03_180349.png)

Untuk loginnya di retep.sh:
1. Login berhasil 

![Screenshot_2023-03-03_180620](/uploads/3a36afc6300e946ea7830ba588252965/Screenshot_2023-03-03_180620.png)

2. Password salah 

![Screenshot_2023-03-03_180833](/uploads/80f39c2ada3156724f3f577b8e32907c/Screenshot_2023-03-03_180833.png)

3. Username tidak terdaftar

![Screenshot_2023-03-03_181003](/uploads/937f25a1586cbd4abaa70ddad6b436ad/Screenshot_2023-03-03_181003.png)

Untuk percobaan login dan register akan dilihat pada log.txt :
![Screenshot_2023-03-03_181422](/uploads/bbf0005a4cecbc7774bde83ab0825d5d/Screenshot_2023-03-03_181422.png)

## Nomor 4

Pada soal nomor 4 kita diharuskan untuk melakukan backup file syslog pada komputer kita dan file tersebut harus dienkripsi dengan string manipulation yang sesuai kententuan yang tertera pada soal, setelah itu kita juga harus membuat script dekripsinya juga. Untuk poin 4A akan menjelaskan enkripsi dan 4B akan menjelaskan dekripsi.



### 4.A

Berikut adalah script enkripsi-nya : 

```bash 
#!/bin/bash

now=$(date +"%H:%M_%d-%m-%Y")
filename="$now.txt"

hour=$(date +%H | sed 's/^0//')

message=$(cat /var/log/syslog)

encrypted=""
for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 + $hour) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 + $hour) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    encrypted="${encrypted}${char}"
done

echo -e "$encrypted" > "$filename"

echo "0 */2 * * * $USER /home/kali/log_encrypt.sh" | crontab -
```
Dan berikut adalah penjelasan-nya :

Baris ini mendefinisikan variabel now yang berisi waktu saat ini dalam format jam:menit_tanggal-bulan-tahun, dan variabel filename yang diisi dengan nilai waktu saat ini yang ditambahkan dengan ekstensi .txt.

``` bash
now=$(date +"%H:%M_%d-%m-%Y")
filename="$now.txt"
```

Baris ini mendefinisikan variabel hour yang berisi waktu saat ini dalam format 24-jam tanpa angka 0 di depan (jika ada). Misalnya, jika waktu saat ini adalah 09:30, maka variabel hour akan berisi nilai 9. Perintah sed menghilangkan angka 0 di depan.

``` bash 
hour=$(date +%H | sed 's/^0//')
```

Baris ini membaca isi file /var/log/syslog ke dalam variabel message.

``` bash
message=$(cat /var/log/syslog)
```

Baris-baris ini mengenkripsi pesan dengan menggunakan metode Caesar cipher (geser huruf). Setiap karakter diubah dengan karakter yang berada beberapa posisi di sebelah kanan di dalam abjad (dalam hal ini, sebanyak $hour karakter di sebelah kanan dalam abjad). Jika karakter bukan huruf, karakter tersebut akan diabaikan. Karakter yang dihasilkan dari enkripsi kemudian ditambahkan ke variabel encrypted. Proses enkripsi ini diulang untuk setiap karakter dalam pesan yang dibaca dari file /var/log/syslog.

```bash
encrypted=""
for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 + $hour) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 + $hour) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    encrypted="${encrypted}${char}"
done
```

Baris ini menuliskan isi variabel encrypted ke dalam file teks yang dinamai sesuai dengan waktu saat ini.

```bash
echo -e "$encrypted" > "$filename"
```

Baris ini menambahkan entri ke crontab untuk menjalankan skrip ini setiap dua jam. Crontab adalah sistem penjadwalan tugas di Linux. Parameter "0 */2 * * *" menandakan bahwa skrip harus dijalankan pada setiap "jam 0" dan "menit 0" setiap dua jam (misalnya

```bash
echo "0 */2 * * * $USER /home/kali/log_encrypt.sh" | crontab -
```


### 4.B

Berikut adalah script dekripsi-nya : 

```bash
#!/bin/bash

filename="$1"
hour=${filename:0:2}

if [[ ! -f "$filename" ]]; then
    echo "File $filename tidak ditemukan."
    exit 1
fi

message=$(cat "$filename")

decrypted=""

for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 - $hour + 26) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 - $hour + 26) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    decrypted="${decrypted}${char}"
done

echo -e "$decrypted"
```
Dan berikut adalah penjelasan-nya :

baris ini memasukkan argumen file ke variabel filename, lalu mengambil 2 digit pertama dari filename untuk memasukkan jam ke variabel hour.

```bash
filename="$1"
hour=${filename:0:2}
```

Baris ini memeriksa apakah file yang dimasukkan sebagai argumen ditemukan. Jika file tidak ditemukan, maka script akan menampilkan pesan error dan keluar dengan status kode 1.

```bash
if [[ ! -f "$filename" ]]; then
    echo "File $filename tidak ditemukan."
    exit 1
fi
```

Baris ini membaca isi file yang dimasukkan sebagai argumen dan memasukkannya ke variabel message.

```bash
message=$(cat "$filename")
```

Baris ini melakukan perulangan untuk setiap karakter di dalam pesan. Jika karakter adalah huruf alfabet, maka karakter akan dienkripsi menggunakan rumus Caesar cipher. Kemudian, karakter hasil enkripsi akan dimasukkan ke variabel decrypted.

```bash
for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 - $hour + 26) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 - $hour + 26) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    decrypted="${decrypted}${char}"
done
```

Baris ini menampilkan pesan hasil enkripsi yang disimpan di variabel decrypted. Pilihan "-e" digunakan untuk mengeksekusi karakter khusus seperti "\n" untuk membuat baris baru.

```bash 
echo -e "$decrypted"
```


### 4.Hasil

1. Pertama Buka menggunakan Linux, untuk saya menggunakan Kali Linux. Lalu kita cek syslog kita dengan menggunakan perintah 

```
cat /var/log/syslog
```
![cat_sislog](/uploads/a3202a64d6068cef243ec91133d64b31/cat_sislog.jpg)

2. Setelah itu masukan perintah 
```
bash log_encrypt.sh
```

![enkrip](/uploads/aa4b8940772b29baf153d35a0547c9c2/enkrip.jpg)

3. Cek di folder file backup syslog yang sudah di enkripsi, lalu buka file tersebut dan cek apakah sudah ter-enkripsi dengan benar

![file_enkrip](/uploads/9a036cdeb62e9f8d3bfc7dde4c2a29a5/file_enkrip.jpg)

4. Lalu ke langkah selanjutnya, yaitu mendekripsi file backup tersebut dengan menjalankan perintah 

```
bash log_decrypt.sh {nama file backup-nya}
```
setelah menjalankan perintah diatas, output yang kita dapatkan adalah syslog yang sudah di dekripsi

![dekrip](/uploads/bf9fe29846d3dcd15ef569d9e9fe16e5/dekrip.jpg)




## Kendala
1. Cukup lama stuck dibagian cron karena tidak jalan-jalan tapi akhirnya bisa jalan juga
2. Cukup lama pada saat menjalankan perintah "bash log_encrypt.sh" dikarenakan syslog pada komputer banyak. Solusi, pada code `message=$(cat /var/log/syslog)` diganti menjadi `message=$(tail n-10 /var/log/syslog)` jadi syslog yang dienkripsi hanya 10 baris terakhir saja, jadi tidak terlalu lama.
3. Sempat terjadi masalah pada laptop, tidak bisa membuka linux. Solusi, di setting melalui BIOS laptop.
