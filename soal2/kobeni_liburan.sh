#!/bin/bash

img_url="https://wallpaperaccess.com/full/21965.jpg"
curr_hour=`date +"%-H"`
if [ $curr_hour = 0 ];
then curr_hour=1
fi

curr_dir=`pwd`
latest_kumpulan=0
if [ $(find . -type d -name "kumpulan_*" | wc -l) -gt 0 ]; then
    latest_kumpulan=$(( $(ls -1 | awk -F '_' '/kumpulan/ {print $2}' | wc -l ) + 1 ))
else 
    latest_kumpulan=1
fi

mkdir "kumpulan_$((latest_kumpulan))"

(crontab -l; echo "0 */10 * * * if [ \$(ls "$curr_dir/kumpulan_$latest_kumpulan" | wc -l) -lt $curr_hour ]; then wget -O $curr_dir/kumpulan_$latest_kumpulan/perjalanan_\$(( \$(ls -1 "$curr_dir/kumpulan_$latest_kumpulan" | wc -l) + 1 )).jpeg $img_url; fi") | crontab -
(crontab -l; echo "0 0 * * * cd $curr_dir && zip -j devil_$latest_kumpulan.zip kumpulan_$latest_kumpulan/*") | crontab -
