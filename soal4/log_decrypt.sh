#!/bin/bash

filename="$1"
hour=${filename:0:2}

if [[ ! -f "$filename" ]]; then
    echo "File $filename tidak ditemukan."
    exit 1
fi

message=$(cat "$filename")

decrypted=""

for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 - $hour + 26) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 - $hour + 26) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    decrypted="${decrypted}${char}"
done

echo -e "$decrypted"
