#!/bin/bash

now=$(date +"%H:%M_%d-%m-%Y")
filename="$now.txt"

hour=$(date +%H | sed 's/^0//')

message=$(cat /var/log/syslog)

encrypted=""
for ((i=0; i<${#message}; i++)); do
    char="${message:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 + $hour) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 + $hour) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    encrypted="${encrypted}${char}"
done

echo -e "$encrypted" > "$filename"

echo "0 */2 * * * $USER /home/kali/log_encrypt.sh" | crontab -
