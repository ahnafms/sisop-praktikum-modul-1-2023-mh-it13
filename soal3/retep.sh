#!/bin/bash

# Input username
echo "Enter username:"
read username

# Check if username exists in users.txt
if ! grep -q "^${username}:" ~/users/users.txt; then
    echo "Username does not exist."
    echo "LOGIN: ERROR Failed login attempt on user ${username} $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Input password
echo "Enter password:"
read -s password

# Check if password matches with the one in users.txt
if ! grep -q "^${username}:${password}$" ~/users/users.txt; then
    echo "Password is incorrect."
    echo "LOGIN: ERROR Failed login attempt on user ${username} $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Login successful
echo "Welcome, ${username}!"
echo "LOGIN: INFO User ${username} logged in $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
