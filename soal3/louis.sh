#!/bin/bash

# Input username
echo "Enter username:"
read username

# Check if username already exists in users.txt
if grep -q "^${username}:" ~/users/users.txt; then
    echo "Username already exists."
    echo "REGISTER: ERROR User already exists $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
    exit 1
fi

# Input password and check if it meets the requirements
while true; do
    echo "Enter password (minimum 8 characters, at least 1 uppercase and 1 lowercase letter, alphanumeric, cannot contain username, chicken, or ernie):"
    read -s password

    # Check length
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long."
        continue
    fi

    # Check for at least 1 uppercase letter
    if ! [[ ${password} =~ [A-Z] ]]; then
        echo "Password must contain at least 1 uppercase letter."
        continue
    fi

    # Check for at least 1 lowercase letter
    if ! [[ ${password} =~ [a-z] ]]; then
        echo "Password must contain at least 1 lowercase letter."
        continue
    fi

    # Check for alphanumeric characters only
    if ! [[ ${password} =~ ^[a-zA-Z0-9]+$ ]]; then
        echo "Password must contain alphanumeric characters only."
        continue
    fi

    # Check if it contains username, chicken, or ernie
    if [[ ${password} =~ ${username} || ${password} =~ "chicken" || ${password} =~ "ernie" ]]; then
        echo "Password cannot contain username, chicken, or ernie."
        continue
    fi

    break
done

# Add user to users.txt
echo "${username}:${password}" >> ~/users/users.txt
echo "User ${username} has been registered successfully."
echo "REGISTER: INFO User ${username} registered successfully $(date +"%y/%m/%d %H:%M:%S")" >> ~/users/log.txt
